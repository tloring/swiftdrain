//
//  main.swift
//  SwiftDrain
//
//  Created by Thom Loring on 6/11/14.
//  Copyright (c) 2014 TLoring, LLC. All rights reserved.
//

import Foundation

// Uncommenting this line will cause SourceKitService process to consume all memory in about 1 minute on my machine
// MacBook Pro Retina, 15-inch, Late 2013;  2.3 GHz Intel Core i7; 16 GB 1600 MHz DDR3;  OS X 10.9.3 (13D65)

// let array_of_dicts = [["string":"1", "float":1.0, "float2":1.0],["string":"2", "float":-2.0, "float2":1.0]]

// Starts draining memory fast; SourceKitService spikes CPU >250%
// As soon as the code is *uncommented*, SourceKitService kicks in
// "Indexing | Processing files" appears in the status bar window.
// Consistently brings machine Free Memory down to 0%

//let array_of_dicts = [
//    ["string":"1", "float":1.0, "float2":1.0],
//    ["string":"2", "float":-2.0, "float2":1.0]
//]

// Starts draining memory slower

//let array_of_dicts = [
//    ["string":"1", "float":-1.0, "float2":1.0],
//    ["string":"2", "float":2.0, "float2":1.0]
//]

//let array_of_dicts = [
//    ["string":"1", "int":1, "int2":1],
//    ["string":"2", "int":-2, "int2":1]
//]

// Various permutations OK...

// OK
//let array_of_dicts = [
//    ["string":"1", "int":1, "int2":1],
//    ["string":"2", "int":2, "int2":1]
//]

// OK
//let array_of_dicts = [
//    ["float":1.0],
//    ["float":-2.0]
//]


// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0],
//    ["string":"2", "float":2.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "string2":"1.0", "string3":"1.0"],
//    ["string":"2", "string2":"2.0", "string3":"1.0"]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0],
//    ["string":"2", "float":2.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "string2":"1.0", "float2":1.0],
//    ["string":"2", "string2":"2.0", "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"2", "float":2.0, "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0, "float2":1.0],
//    ["string":"2", "float":2.0, "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"2", "float":-2.0, "float2":-1.0]
//]

println(array_of_dicts)
