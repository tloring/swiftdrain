The following code brings my entire machine down to 0% Free Memory when *uncommented*. Not during a compilation, rather just uncommenting it, or opening the project with it already uncommented, it's driving `SourceKitService`'s CPU % usage > 250%. When this code is present, "Indexing | Processing files" in the status window, and free memory is in free-fall.

It was initially discovered in an iOS project, but this is consistently reproducible using a simple Command Line Tool project. 
 
```swift
// Starts draining memory fast; SourceKitService spikes CPU >250%
// As soon as the code is *uncommented*, SourceKitService kicks in
// "Indexing | Processing files" appears in the status bar window.
// Consistently brings machine Free Memory down to 0%

//let array_of_dicts = [
//    ["string":"1", "float":1.0, "float2":1.0],
//    ["string":"2", "float":-2.0, "float2":1.0]
//]
```

This also happens in the Swift command line REPL, ie `xcrun swift`, except it is `lldb` which spikes the CPU and memory.

```swift
$ xcrun swift
Welcome to Swift!  Type :help for assistance.
  1> import Foundation
  2> let array_of_dicts = [ ["string":"1", "float":1.0, "float2":1.0], ["string":"2", "float":-2.0, "float2":1.0] ]
```

I have tried numerous permutations. The key seems to be array of dicts, with 2 or more array elements, with the 2nd dictionary in the array containing a negative float.

```swift
// Starts draining memory slower

//let array_of_dicts = [
//    ["string":"1", "float":-1.0, "float2":1.0],
//    ["string":"2", "float":2.0, "float2":1.0]
//]

//let array_of_dicts = [
//    ["string":"1", "int":1, "int2":1],
//    ["string":"2", "int":-2, "int2":1]
//]

// Various permutations OK...

// OK
//let array_of_dicts = [
//    ["string":"1", "int":1, "int2":1],
//    ["string":"2", "int":2, "int2":1]
//]

// OK
//let array_of_dicts = [
//    ["float":1.0],
//    ["float":-2.0]
//]


// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0],
//    ["string":"2", "float":2.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "string2":"1.0", "string3":"1.0"],
//    ["string":"2", "string2":"2.0", "string3":"1.0"]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0],
//    ["string":"2", "float":2.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "string2":"1.0", "float2":1.0],
//    ["string":"2", "string2":"2.0", "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"2", "float":2.0, "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"1", "float":1.0, "float2":1.0],
//    ["string":"2", "float":2.0, "float2":1.0]
//]

// OK
//let array_of_dicts = [
//    ["string":"2", "float":-2.0, "float2":-1.0]
//]
```
